import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { AuthService } from './auth.service';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {
  
    constructor(public auth: AuthService) {}
  
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      return next.handle(request).catch((error: HttpErrorResponse) => {
        if (error.status === 401) {
          return this.auth.logoutUser()
          // const http = this.injector.get(HttpClient);
          // return http.post(environment.base_uri + 'auth/refresh', {}).flatMap((response) => {
          //   // get token from response.
          //   // put token in localstorage.
          //   // add token to the request.
  
          //   // Do the request again with the new token.
            //return next.handle(request);
          // });
        }
  
        return Observable.throw(error);
      });
    }
  }