import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Http, Response } from '@angular/http';
import { Injectable } from "@angular/core";
import { ServiceUrl } from './servicesUrl';


@Injectable()
export class TransactionService {

    serviceUrl = this.servicesUrl.getServiceUrl('dev')
    
    constructor(private http: HttpClient, public servicesUrl: ServiceUrl) { }

    // ==============================================================================================
    
    /**
     * ===============================================
     *  GET REQUESTS
     * ===============================================
     **/

    // ==============================================================================================

    getTransactionsMonitoringData() {
        return this.http
            .get(`${this.serviceUrl}/transaction-monitoring`)
            .map((response: Response) => response)
            .catch(this.handleError)
    }


    /**
     * ===============================================
     *  ERROR HANDLER FOR REQUESTS
     * ===============================================
     **/


    private handleError(error: any) {
        const errorMsg = error.error.response === undefined || '' ? "Sorry unable to process. Please try again." : error.error.response.userMessage
        return Observable.throw(errorMsg)
    }

}