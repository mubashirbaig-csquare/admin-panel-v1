import { HttpClient } from '@angular/common/http';
import { Response } from '@angular/http';
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs/Observable';
import { ServiceUrl } from './servicesUrl';


@Injectable()
export class PasswordService {
    constructor(private http: HttpClient, private servicesUrl: ServiceUrl) {}
    serviceUrl = this.servicesUrl.getServiceUrl('dev')

    updatePassword(password, userId) {
        return this
                .http
                .put(`${this.serviceUrl}/users/${userId}/password`, {password})
                .map((response: Response) => response)
                .catch(this.handleError)
    }

    private handleError(error: any) {
        const errorMsg = error.error.response === undefined || '' ? "Sorry unable to process. Please try again." : error.error.response.userMessage
        return Observable.throw(errorMsg)
    }
}