import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ServiceUrl } from './servicesUrl';
import { AuthService } from './auth.service';


@Injectable()
export class UserService {
    constructor(private servicesUrl: ServiceUrl, private authService: AuthService, public http: HttpClient) { }
    serviceUrl = this.servicesUrl.getServiceUrl('dev')

    getUsers() {
        return this
            .http
            .get(`${this.serviceUrl}/users`)
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    getUserById(userId) {
        return this
            .http
            .get(`${this.serviceUrl}/users/${userId}`)
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    insertUser(username, fullname, email, mobile, roles) {
        return this
            .http
            .post(`${this.serviceUrl}/users`, { username, fullname, email, mobile, roles })
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    updateUser(username, fullname, email, mobile, roles, userId, isEnabled) {
        return this
            .http
            .put(`${this.serviceUrl}/users/${userId}`, { username, fullname, email, mobile, roles, isEnabled })
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    resetPassword(userId, email) {
        return this
            .http
            .put(`${this.serviceUrl}/users/${userId}/resetPassword`, { email })
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    getRoles() {
        return this
            .http
            .get(`${this.serviceUrl}/roles`)
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    private handleError(error: any) {
        const errorMsg = error.error.response === undefined || '' ? "Sorry unable to process. Please try again." : error.error.response.userMessage
        return Observable.throw(errorMsg)
    }

}