import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Http, Response, Headers } from '@angular/http';
import { Injectable } from "@angular/core";
import { AuthService } from './auth.service';
import { ServiceUrl } from './servicesUrl';


@Injectable()
export class TerminalService {

    serviceUrl = this.servicesUrl.getServiceUrl('dev')
    
    constructor(private http: HttpClient, private authService: AuthService, public servicesUrl: ServiceUrl) { }

    // ==============================================================================================
    
    /**
     * ===============================================
     *  GET REQUESTS
     * ===============================================
     **/

    // ==============================================================================================

    getTerminals() {
        return this.http
            .get(`${this.serviceUrl}/terminals`)
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    getTerminalById(terminalId) {
        return this.http
            .get(`${this.serviceUrl}/terminals/${terminalId}`)
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    getTerminalDevices() {
        return this.http
            .get(`${this.serviceUrl}/devices`)
            .map((response: Response) => response)
            .catch(this.handleError)
    }


    getMonitoringData() {

    }

    getTerminalIssues() {
        return this.http
            .get(`${this.serviceUrl}/terminals/issue`)
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    getCities() {
        return this.http
            .get(`${this.serviceUrl}/cities`)
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    getRegions() {
        return this.http
            .get(`${this.serviceUrl}/regions`)
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    getBranches() {
        return this.http
            .get(`${this.serviceUrl}/branches`)
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    getLicenses() {
        return this.http
            .get(`${this.serviceUrl}/licenses`)
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    // ==============================================================================================

    /**
    * ===============================================
    *  POST REQUESTS
    * ===============================================
    **/

    // ==============================================================================================

    insertNewTerminal(terminalName, ipAddress, macAddress, keepAliveInterval, branchId, licenseId, licenseKey) {
        const userId = this.authService.getCurrentlyLoggedInUserId()
        return this
            .http
            .post(`${this.serviceUrl}/terminals`,
            { terminalName, ipAddress, macAddress, keepAliveInterval, branchId, licenseId, licenseKey, userId })
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    // ==============================================================================================

    /**
   * ===============================================
   *  PUT REQUESTS
   * ===============================================
   **/

    // ==============================================================================================

    updateNewTerminalSerialNumbers(terminalId, serialNumbers) {
        return this
            .http
            .put(`${this.serviceUrl}/terminals/${terminalId}/devices`, { serialNumbers })
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    updateTerminal(terminalId, terminalName, ipAddress, macAddress, keepAliveInterval, branchId, licenseId, status, statusChanged, reasonForStatus, terminalStatus) {
        const userId = this.authService.getCurrentlyLoggedInUserId()
        return this
            .http
            .put(`${this.serviceUrl}/terminals/${terminalId}`, { terminalName, ipAddress, macAddress, keepAliveInterval, branchId, licenseId, status, userId, statusChanged, reasonForStatus, terminalStatus })
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    updateDeviceSerialNumber(terminalDeviceId, serialNumber) {
        return this
            .http
            .put(`${this.serviceUrl}/terminals/${terminalDeviceId}/${serialNumber}`, {})
            .map((response: Response) => response)
            .catch(this.handleError)
    }



    updateTerminalIssues(terminalId, userId, comments) {
        return this
            .http
            .put(`${this.serviceUrl}/terminals/${terminalId}/critical-status`, { userId, comments })
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    // ==============================================================================================




    /**
     * ===============================================
     *  ERROR HANDLER FOR REQUESTS
     * ===============================================
     **/


    private handleError(error: any) {
        const errorMsg = error.error.response === undefined || '' ? "Sorry unable to process. Please try again." : error.error.response.userMessage
        return Observable.throw(errorMsg)
    }

}