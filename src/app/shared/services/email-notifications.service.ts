import { Injectable } from '@angular/core';
import { Http, Response, Headers } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { ServiceUrl } from './servicesUrl';
import { AuthService } from './auth.service';


@Injectable()
export class EmailNotificationsService {
    constructor(private servicesUrl: ServiceUrl, private authService: AuthService, public http: HttpClient) { }
    serviceUrl = this.servicesUrl.getServiceUrl('dev')

    getNotificationsList() {
        return this
            .http
            .get(`${this.serviceUrl}/emails-notifications-list`)
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    getNotificationDetailByName(notificationName) {
        return this
            .http
            .get(`${this.serviceUrl}/emails-notification/${notificationName}`)
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    updateNotificationEmails(notificationAlert, emailTo, emailCC)
    {
        return this
        .http
        .put(`${this.serviceUrl}/emails-notification/`, { notificationAlert, emailTo, emailCC })
        .map((response: Response) => response)
        .catch(this.handleError)
    }

    private handleError(error: any) {
        const errorMsg = error.error.response === undefined || '' ? "Sorry unable to process. Please try again." : error.error.response.userMessage
        return Observable.throw(errorMsg)
    }

}