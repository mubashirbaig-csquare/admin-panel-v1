import { Response } from '@angular/http';
import { Injectable } from "@angular/core";
import 'rxjs/Rx'
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { ServiceUrl } from './servicesUrl';


@Injectable()
export class ReportsService {

    serviceUrl = this.servicesUrl.getServiceUrl('dev')

    constructor(private http: HttpClient, public servicesUrl: ServiceUrl) { }


    // ==============================================================================================

    /**
     * ===============================================
     *  GET REQUESTS
     * ===============================================
     **/

    // ==============================================================================================

    getTerminalsLiveUptimeData() {
        return this.http
            .get(`${this.serviceUrl}/terminals-live-uptime`)
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    getTerminalsInventoryStatusData() {
        return this.http
            .get(`${this.serviceUrl}/terminals-inventory-status`)
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    // ==============================================================================================

    /**
     * ===============================================
     *  POST REQUESTS
     * ===============================================
     **/

    // ==============================================================================================

    getAuditLogData(fromDate, toDate) {
        return this.http
            .post(`${this.serviceUrl}/reports/audit-log`, { fromDate, toDate })
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    getAlertsLogData(fromDate, toDate) {
        return this.http
            .post(`${this.serviceUrl}/reports/alerts-log`, { fromDate, toDate })
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    getKioskDetailsData(fromDate, toDate) {
        return this.http
            .post(`${this.serviceUrl}/reports/kiosk-details`, { fromDate, toDate })
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    getKioskSummaryData(fromDate, toDate) {
        return this.http
            .post(`${this.serviceUrl}/reports/kiosk-summary`, { fromDate, toDate })
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    getTransactionDetailsData(fromDate, toDate) {
        return this.http
            .post(`${this.serviceUrl}/reports/transaction-details`, { fromDate, toDate })
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    getTransactionSummaryData(fromDate, toDate) {
        return this.http
            .post(`${this.serviceUrl}/reports/transaction-summary`, { fromDate, toDate })
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    getTerminalUptimeData(fromDate, toDate) {
        return this.http
            .post(`${this.serviceUrl}/reports/terminals-uptime`, { fromDate, toDate })
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    getTerminalCriticalLogsData(fromDate, toDate) {
        return this.http
            .post(`${this.serviceUrl}/reports/terminals-critical-logs`, { fromDate, toDate })
            .map((response: Response) => response)
            .catch(this.handleError)
    }


    /**
     * ===============================================
     *  ERROR HANDLER FOR REQUESTS
     * ===============================================
     **/


    private handleError(error: any) {
        const errorMsg = error.error.response === undefined || '' ? "Sorry unable to process. Please try again." : error.error.response.userMessage
        return Observable.throw(errorMsg)
    }
}