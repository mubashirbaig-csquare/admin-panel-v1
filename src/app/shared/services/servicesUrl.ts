import { Injectable } from "@angular/core";

@Injectable()
export class ServiceUrl{

    public getServiceUrl(env) {
        if(env === 'dev') {
            // return 'http://192.168.1.203:3000/api/v1'
            // return 'http://172.24.24.161:3000/api/v1'     
            return 'http://localhost:3000/api/v1'     
        }      
    }
}