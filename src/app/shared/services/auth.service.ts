import { ServiceUrl } from './servicesUrl';
import { Http, Headers } from '@angular/http'
import { Injectable } from '@angular/core'
import { Observable } from 'rxjs/Observable'
import { Router } from '@angular/router'

@Injectable()
export class AuthService {
    constructor(private http: Http, private router: Router, private servicesUrl: ServiceUrl) {}
    serviceUrl = this.servicesUrl.getServiceUrl('dev')

    signinUser(email: string, password: string) {
        const header = new Headers({'Content-Type': 'application/json'})
        return this
                .http
                .post(`${this.serviceUrl}/authentications`,{email, password},{headers: header})
                .map(
                    (response) => {
                        const data = response.json()
                        localStorage.setItem('auth', JSON.stringify(data))
                        return data
                    }
                )
                .catch(this.handleError)
    }

    isAuthenticated() {
        if(!localStorage.getItem('auth')) {
            this.router.navigate(['/login'])
            return false
        } else {
            return true
        }
    }

    getCurrentlyLoggedInUserId() {
        if(!localStorage.getItem('auth')) {
            this.router.navigate(['/login']);
        } else {
            return JSON.parse(localStorage.getItem('auth')).userId.toString()
        }
    }

    getCurrentToken() {
        if(!localStorage.getItem('auth')) {
            this.router.navigate(['/login']);
        } else {
            return JSON.parse(localStorage.getItem('auth')).token
        }
    }

    logoutUser() {
        localStorage.removeItem('auth')
        return this.router.navigate(['/login'])
    }

    private handleError(error: any) {
        // In a real world app, we might use a remote logging infrastructure
        // We'd also dig deeper into the error to get a better message
        // let errMsg = (error.message) ? error.message :
        //     error.status ? `${error.status} - ${error.statusText}` : 'Server error';
        // console.log(error); // log to console instead
        const errorMsg = error.json().response === undefined || '' ? "Sorry unable to process. Please try again." : error.json().response.userMessage
        return Observable.throw(errorMsg)
    }
    
}