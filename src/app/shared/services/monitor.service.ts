import { Response } from '@angular/http';
import { Injectable } from "@angular/core";
import 'rxjs/Rx'
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { ServiceUrl } from './servicesUrl';


@Injectable()
export class MonitorService {

    serviceUrl = this.servicesUrl.getServiceUrl('dev')

    constructor(private http: HttpClient, public servicesUrl: ServiceUrl) {}

    // ==============================================================================================
    
    /**
     * ===============================================
     *  GET REQUESTS
     * ===============================================
     **/

    // ==============================================================================================

    getTerminalsMonitoringData() {
        return this.http
            .get(`${this.serviceUrl}/terminals/monitoring`)
            .map((response: Response) => response)
            .catch(this.handleError)
    }

    
    /**
     * ===============================================
     *  ERROR HANDLER FOR REQUESTS
     * ===============================================
     **/


    private handleError(error: any) {
        const errorMsg = error.error.response === undefined || '' ? "Sorry unable to process. Please try again." : error.error.response.userMessage
        return Observable.throw(errorMsg)
    }
}