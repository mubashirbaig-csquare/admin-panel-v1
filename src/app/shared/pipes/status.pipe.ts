import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
    name: 'status'
})
export class StatusPipe implements PipeTransform {
    transformedValue = ''
  
    transform(value: any, type: any) {       
        
        switch(type) {
            case 2 : {
                this.terminalsStatusConversion(value)
                break
            }
            case 3 : {
                this.deviceMonitoringStatusConversion(value)
                break
            }
            case 'monitoring-status' : {
                this.terminalMonitoringStatusConversion(value)
                break
            }
            case 'admin-lock' : {
                this.isAdminLock(value)
                break
            }
        }
        return this.transformedValue
    }

    isAdminLock(value) {
        if(value == 0) {
            this.transformedValue = 'No'
        } else if(value == 1) {
            this.transformedValue = 'Yes'
        } 
    }

    terminalMonitoringStatusConversion(value:any) {
        if(value == 0) {
            this.transformedValue = 'Disconnected'
        } else if(value == 1) {
            this.transformedValue = 'Connected'
        }     
    }

    terminalsStatusConversion(value:any) {
        if(value == 0) {
            this.transformedValue = 'In-Active'
        } else if(value == 1) {
            this.transformedValue = 'Active'
        }     
    }

    deviceMonitoringStatusConversion(value:any) {
        switch(value) {
            case 0: {
                this.transformedValue = 'Active'
                break
            }
            case 1: {
                this.transformedValue = 'Warning'
                break
            }
            case 2: {
                this.transformedValue = 'Critical'
                break
            }
        }
    }

}