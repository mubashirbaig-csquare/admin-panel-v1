import { Router } from '@angular/router';
import { Component } from '@angular/core';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent {

    roleName: string
    isActive = false;
    showMenu = '';

    constructor(private router : Router)  {
        this.roleName = JSON.parse(localStorage.getItem('auth')).roleName
        // if(this.router.url.includes('/users')) {
        //     this.showMenu = 'pages'
        // }
        if(this.router.url.includes('/reports')) {
            this.showMenu = 'reports'
        }
    }

    eventCalled() {
        this.isActive = !this.isActive;
    }

    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    closeExpandOnOtherLinkClick() {        
        this.showMenu = '0';
    }

}
