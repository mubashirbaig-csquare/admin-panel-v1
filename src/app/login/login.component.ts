import { NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { Router } from '@angular/router';
import { AuthService } from '../shared/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
  email: string
  password: string
  errorMsg = ''
  loading

  constructor(public router: Router, private authService: AuthService) { }

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    this.loading = true
    this.email = form.value.email
    this.password = form.value.password
    if(form.valid) {
      this.authService.signinUser(this.email,  this.password)
      .subscribe(data =>{
        this.loading = false
        this.router.navigate(['/'])
      }, error => {
        this.loading = false
        this.errorMsg = error//.response.userMessage
      })
    }
  }
  
}
