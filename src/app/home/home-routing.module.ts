import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { HomeComponent } from './home.component';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
        children: [
            { path: '', pathMatch: 'full', redirectTo: 'terminals-monitor' },
            { path: 'users', loadChildren: './users/users.module#UsersModule' },
            { path: 'password', loadChildren: './password/password.module#PasswordModule' },
            { path: 'terminals', loadChildren: './terminals/terminals.module#TerminalsModule' },
            { path: 'terminals-monitor', loadChildren: './monitoring/monitoring.module#MonitoringModule' },
            { path: 'issues-logs', loadChildren: './issues-logs/issues-logs.module#IssuesLogsModule' },
            { path: 'reports', loadChildren: './reports/reports.module#ReportsModule' },
            { path: 'transaction-monitor', loadChildren: './transaction-monitoring/transaction-monitoring.module#TransactionMonitoringModule' },
            { path: 'terminals-live-uptime', loadChildren: './terminals-live-uptime/terminals-live-uptime.module#TerminalsLiveUptimeModule' },
            { path: 'terminals-inventory-status', loadChildren: './terminals-inventory-status/terminals-inventory-status.module#TerminalsInventoryStatusModule' },
            { path: 'email-notifications', loadChildren: './email-notifications/email-notifications.module#EmailNotificationsModule' }
        ]
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HomeRoutingModule { }