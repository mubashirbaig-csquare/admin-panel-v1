import { NotificationService } from 'ng2-notify-popup';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

import { TerminalService } from '../../shared/services/terminal.service';
import { AuthService } from '../../shared/services/auth.service';
import { routerTransition } from '../../router.animations';

@Component({
  selector: 'app-issues-logs',
  templateUrl: './issues-logs.component.html',
  styleUrls: ['./issues-logs.component.scss'],
  animations: [routerTransition()]
})
export class IssuesLogsComponent implements OnInit {

  loading = false;
  display: boolean
  error = ''
  terminals = []

  constructor(private terminalsService: TerminalService, private router: Router, private notify: NotificationService) { }

  ngOnInit() {
    this.fetchIssues()
  }

  statusUpdated(data) {
    this.loading = true;
    const userId = JSON.parse(localStorage.getItem('auth')).userId
    this.terminalsService.updateTerminalIssues(data.terminalId, userId, data.comment)
      .first()
      .subscribe(data => {
        this.fetchIssues()
        //this.display = true;
        this.notify.show(data.response, { position:'top', duration:'2000', type: 'success' });
        // window.location.reload();
      }, error => {
        //this.error = error
        //this.display = false;
        this.loading = false;
        this.notify.show(error, { position:'top', duration:'5000', type: 'error' });
      })
  }

  fetchIssues() {
    this.loading = true;
    this.terminalsService.getTerminalIssues()
      .first()
      .subscribe(data => {
        this.terminals = data.response
        this.loading = false;
        // this.display = true
      }, error => {
        this.notify.show(error, { position:'top', duration:'5000', type: 'error' });
        // this.error = error
        // this.display = false
        this.loading = false;
      })
  }

}
