import { NgxPaginationModule } from 'ngx-pagination';
import { LoadingModule } from 'ngx-loading';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { IssuesLogsRoutingModule } from './issues-logs-routing.module';
import { IssuesLogsComponent } from './issues-logs.component';
import { PageHeaderModule } from '../../shared/modules/index';
import { ModalComponent } from './modal/modal.component';
import { PipeModule } from '../../shared/pipes/pipes.module';

@NgModule({
    declarations: [IssuesLogsComponent, ModalComponent],
    imports: [
        CommonModule,
        IssuesLogsRoutingModule,
        PageHeaderModule,
        FormsModule,
        LoadingModule,
        NgxPaginationModule,
        PipeModule,
        NgbModule.forRoot(),
    ]
})
export class IssuesLogsModule {}