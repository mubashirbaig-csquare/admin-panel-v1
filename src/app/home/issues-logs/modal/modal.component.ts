import { FormGroup, Validators, FormControl, NgForm } from '@angular/forms';
import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { TerminalService } from '../../../shared/services/terminal.service';

@Component({
    selector: 'app-modal',
    templateUrl: './modal.component.html',
    styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
    success = ""
    error = ""
    @Input() terminalId: string
    @Output() updateIssueStatus = new EventEmitter<{ terminalId: string, comment: string }>()

    ngOnInit(): void {
    }
    constructor(private modalService: NgbModal, private terminalsService: TerminalService) { }

    open(content) {
        this.modalService.open(content);
    }

    onSubmit(form: NgForm) {
        if (form.valid) {
            // this.success = 'Terminal has been successfully updated!'
            this.error = ''
            this.updateIssueStatus.emit({
                terminalId: this.terminalId,
                comment: form.value.comment
            })
        } else {
            // this.success = ''
            this.error = "Required field!"
        }
    }


    // open(content) {
    //     this.modalService.open(content).result.then((result) => {
    //         //this.closeResult = `Closed with: ${result}`;
    //         //console.log(this.closeResult)
    //     }, (reason) => {
    //         //this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    //         //console.log(this.closeResult)
    //     });
    // }

    // private getDismissReason(reason: any): string {
    //     if (reason === ModalDismissReasons.ESC) {
    //         return 'by pressing ESC';
    //     } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
    //         return 'by clicking on a backdrop';
    //     } else {
    //         return  `with: ${reason}`;
    //     }
    // }
}
