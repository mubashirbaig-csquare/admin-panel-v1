import { NotificationService } from 'ng2-notify-popup';
import { NgForm } from '@angular/forms';
import { Component, OnInit, Input, EventEmitter, Output, ViewChild } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { TerminalService } from '../../../../shared/services/terminal.service';


@Component({
    selector: 'app-edit-serial-number-modal',
    templateUrl: './edit-serial-number-modal.component.html',
    styleUrls: ['./edit-serial-number-modal.component.scss']
})
export class EditSerialNumberModalComponent implements OnInit {

    error = ""
    @Input() terminalDeviceId: string
    @Input() serialNumber: string
    @Output() updateSerialNumber = new EventEmitter<{ terminalDeviceId: string, serialNumber: string }>()

    ngOnInit(): void { }

    constructor(private modalService: NgbModal, private terminalService: TerminalService, private notify: NotificationService) { }

    open(content) {
        this.modalService.open(content)
    }

    onSubmit(form: NgForm) {
        this.error = ''
        if (form.valid) {
            this.updateSerialNumber.emit({
                terminalDeviceId: this.terminalDeviceId,
                serialNumber: form.value.serialNumber
            })
        } else {
            this.error = "Required field!"
        }
    }

}
