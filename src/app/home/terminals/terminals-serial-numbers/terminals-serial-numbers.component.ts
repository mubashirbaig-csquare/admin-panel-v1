import { NotificationService } from 'ng2-notify-popup';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { TerminalService } from '../../../shared/services/terminal.service';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-terminals-serial-numbers',
  templateUrl: './terminals-serial-numbers.component.html',
  styleUrls: ['./terminals-serial-numbers.component.scss'],
  animations: [routerTransition()]
})
export class TerminalsSerialNumbersComponent implements OnInit, OnDestroy {

  devices = []
  terminalId
  timeoutId

  @ViewChild('f') terminalForm: NgForm
  constructor(private terminalsService: TerminalService, private route: ActivatedRoute, private notify: NotificationService, private router: Router) { }

  ngOnInit() {
    this.route.params
      .subscribe(
      (params: any) => {
         this.terminalId = params['terminalId'];
      });
    this.loadDevices();
  }

  onTerminalSerialNumberFormSubmit() {   
    if(this.terminalForm.valid) {
      let requestParams = []
      for(let device of this.devices) {
        requestParams.push({
          "deviceId": device.DeviceID, 
          "serialNumber":this.terminalForm.controls[`${device.DeviceID}`].value})
      }
      this.terminalsService.updateNewTerminalSerialNumbers(this.terminalId, requestParams)
      .subscribe(data => {      
        this.notify.show(data.response, { position:'top', duration:'2000', type: 'success' });  
        this.terminalForm.reset()
        this.timeoutId =  setTimeout((router: Router) => {
          this.router.navigate(['/terminals']);
        }, 2000);
      }, error => {
        this.notify.show(error, { position:'top', duration:'5000', type: 'error' });
      })
    }
  }

  private loadDevices() {
    this.terminalsService.getTerminalDevices()
      .subscribe(data => {
        this.devices = data.response;
      }, error => {
        this.notify.show(error, { position:'top', duration:'5000', type: 'error' });
      });
  }

  ngOnDestroy() {
    clearTimeout(this.timeoutId)
  }
}
