import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { TerminalComponent } from './terminal/terminal.component';
import { TerminalsListComponent } from './terminals-list/terminals-list.component';
import { TerminalsComponent } from './terminals.component';
import { TerminalsSerialNumbersComponent } from './terminals-serial-numbers/terminals-serial-numbers.component';
import { EditTerminalComponent } from './edit-terminal/edit-terminal.component';

const routes: Routes = [
    { 
        path: "", 
        component: TerminalsComponent, 
        children: [
            { path: "", component: TerminalsListComponent },
            { path: "new/:terminalId/serial-numbers", component: TerminalsSerialNumbersComponent },
            { path: "new", component: TerminalComponent },
            { path: ":id/edit", component: EditTerminalComponent }
           
        ] 
    }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TerminalsRoutingModule {}