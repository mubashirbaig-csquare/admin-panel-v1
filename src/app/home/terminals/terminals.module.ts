import { BootstrapSwitchModule } from 'angular2-bootstrap-switch';
import { LoadingModule } from 'ngx-loading';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import {NgxPaginationModule} from 'ngx-pagination';


import { TerminalsRoutingModule } from './terminals-routing.module';
import { PageHeaderModule } from './../../shared/modules/page-header/page-header.module';
import { PipeModule } from './../../shared/pipes/pipes.module';
import { TerminalsComponent } from './terminals.component';
import { TerminalsListComponent } from './terminals-list/terminals-list.component';
import { TerminalComponent } from './terminal/terminal.component';
import { TerminalsSerialNumbersComponent } from './terminals-serial-numbers/terminals-serial-numbers.component';
import { EditSerialNumberModalComponent } from './terminal/edit-serial-number-modal/edit-serial-number-modal.component';
import { NewTerminalComponent } from './new-terminal/new-terminal.component';
import { EditTerminalComponent } from './edit-terminal/edit-terminal.component';



@NgModule({
    imports: [
        CommonModule,
        TerminalsRoutingModule,
        PageHeaderModule,
        FormsModule,
        Ng2SearchPipeModule,
        ReactiveFormsModule,
        LoadingModule,
        NgxPaginationModule,
        PipeModule,
        BootstrapSwitchModule.forRoot(),
        NgbModule.forRoot(),

    ],
    declarations: [
        TerminalComponent,
        TerminalsListComponent,
        TerminalsComponent,
        TerminalsSerialNumbersComponent,
        EditSerialNumberModalComponent,
        NewTerminalComponent,
        EditTerminalComponent
    ]
})
export class TerminalsModule {}