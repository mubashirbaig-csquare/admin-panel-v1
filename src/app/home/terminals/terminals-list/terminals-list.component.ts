import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';

import { TerminalService } from './../../../shared/services/terminal.service';
import { AuthService } from '../../../shared/services/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-terminals-list',
  templateUrl: './terminals-list.component.html',
  styleUrls: ['./terminals-list.component.scss'],
  animations: [routerTransition()]
})
export class TerminalsListComponent implements OnInit {
  
  loading = false;
  searchTerm = ''
  display = true
  error = ''
  terminals = []

  constructor(private terminalsService: TerminalService, private router: Router) { }

  ngOnInit() {
    this.loading = true;

    // setTimeout(() => {    //<<<---    using ()=> syntax
    this.terminalsService.getTerminals()
      .first()
      .subscribe(data => {
        this.terminals = data.response
        this.loading = false;
      }, error => {
        this.error = error 
        this.display = false;
        this.loading = false;
      })
    // }, 10000);


  }

  clearSearch() {
    this.searchTerm = ''
  }

}
