import { NgForm } from '@angular/forms';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { routerTransition } from '../../../router.animations';
import { TerminalService } from '../../../shared/services/terminal.service';
import { NotificationService } from 'ng2-notify-popup';

@Component({
  selector: 'app-edit-terminal',
  templateUrl: './edit-terminal.component.html',
  styleUrls: ['./edit-terminal.component.scss'],
  animations: [routerTransition()]
})
export class EditTerminalComponent implements OnInit, OnDestroy {

  cities
  branches
  regions
  enableSwitch = false
  previousLockStatus
  licences
  editMode = false;
  id: number;
  filteredCities = []
  filteredBranches = []
  displayCity = false
  displayBranch = false
  devices = []
  terminalId
  defaultLicenseId
  error = ""
  timeoutId: any
  success = ""


  @ViewChild('f') terminalForm: NgForm

  constructor(private route: ActivatedRoute,
    private router: Router,
    private terminalService: TerminalService,
    private notify: NotificationService) { }

  ngOnInit() {

    this.getRegionsCitiesBranches()

    this.route.params
      .subscribe(
      (params: any) => {
        this.terminalId = params['id'];
        if (this.terminalId !== undefined) {
          this.editMode = true
        }
      })
      this.populatingFormInEditMode();
  }

  private populatingFormInEditMode() {
    this.terminalService.getTerminalById(this.terminalId)
      .subscribe(data => {
        this.onRegionChange(data.response[0].RegionID)
        this.terminalForm.form.patchValue({
          terminalName: data.response[0].TerminalName,
          ipAddress: data.response[0].IpAddress,
          macAddress: data.response[0].MacAddress,
          keepAliveTimer: data.response[0].KeepAliveInterval,
          region: data.response[0].RegionID,
          city: data.response[0].CityID,
          branch: data.response[0].BranchID,
          licence: `${data.response[0].LicenseID}|${data.response[0].LicenseKey}`
        });

        if (data.response[0].isAdminLock == 1) {
          this.enableSwitch = true
        } else {
          this.enableSwitch = false
        }
        this.previousLockStatus = this.enableSwitch
        this.devices = []
        for (let device of data.response) {
          this.devices.push({
            deviceName: device.DeviceName,
            serialNumber: device.SerialNo,
            terminalDeviceId: device.TerminalDeviceID
          })
        }
      }, error => {
        this.notify.show(error, { position:'top', duration:'5000', type: 'error' });
      });
  }

  getRegionsCitiesBranches() {
    this.setCities();
    this.setRegions();
    this.setBranches();
    this.setLicenses()
  }

  private setBranches() {
    this.terminalService.getBranches()
      .subscribe(data => {
        this.branches = data.response
      }, err => {
        this.router.navigate(['/']);
      });
  }

  private setRegions() {
    this.terminalService.getRegions()
      .subscribe(data => {
        this.regions = data.response
      }, err => {
        this.router.navigate(['/']);
      });
  }

  private setCities() {
    this.terminalService.getCities()
      .subscribe(data => {
        this.cities = data.response
      }, err => {
        this.router.navigate(['/']);
      });
  }

  private setLicenses() {
    this.terminalService.getLicenses()
      .subscribe(data => {
        this.licences = data.response
        if (this.licences.length > 0) {
          this.defaultLicenseId = this.licences[0].LicenseID
        }
      }, err => {
        this.router.navigate(['/']);
      });
  }

  onRegionChange(data) {
    this.displayBranch = false
    this.filteredCities = []
    this.filteredCities = this.cities.filter(city => city.RegionID == data)
    this.displayCity = true
    if (this.filteredCities.length > 0) {
      this.onCityChange(this.filteredCities[0].CityID)
   }
  }

  onCityChange(data) {
    this.filteredBranches = []
    this.filteredBranches = this.branches.filter(branch => branch.CityID == data)
    this.displayBranch = true
  }

  onChange(data) {
    this.enableSwitch = data
  }

  ngOnDestroy() {
    clearTimeout(this.timeoutId)
  }

  reloadTab(data) {
    if (data.target.id == 'firstTab') {
      this.ngOnInit()
    }
  }

  updateDevices() {
    this.terminalService.getTerminalById(this.terminalId)
    .subscribe(data => {
      this.devices = []
      for (let device of data.response) {
        this.devices.push({
          deviceName: device.DeviceName,
          serialNumber: device.SerialNo,
          terminalDeviceId: device.TerminalDeviceID
        })
      }
    }, error => {
      this.notify.show(error, { position:'top', duration:'5000', type: 'error' });
    });
  }

  onTerminalFormSubmit() {    
    if (this.terminalForm.valid && this.filteredCities.length !== 0 && this.filteredBranches.length !== 0) {
      const terminalName = this.terminalForm.controls['terminalName'].value.toString()
      const ipAddress = this.terminalForm.controls['ipAddress'].value.toString()
      const macAddress = this.terminalForm.controls['macAddress'].value.toString()
      const keepAliveTimer = this.terminalForm.controls['keepAliveTimer'].value.toString()
      const region = this.terminalForm.controls['region'].value.toString()
      const city = this.terminalForm.controls['city'].value.toString()
      const branch = this.terminalForm.controls['branch'].value.toString()
      const licenseId = this.terminalForm.controls['licence'].value.split('|')[0].toString()
      const licenseKey = this.terminalForm.controls['licence'].value.split('|')[1].toString()
      const status = this.enableSwitch == true ? "1" : "0"
      this.updateTerminal(this.terminalId, terminalName, ipAddress, macAddress, keepAliveTimer, branch, licenseId, status)
    } else {
      this.notify.show('Form is incomplete.', { position:'top', duration:'5000', type: 'error' });
    }
  }

  updateTerminal(terminalId, terminalName, ipAddress, macAddress, keepAliveTimer, branch, licenseId, status) {
    let reasonForStatus = 'Connected'
    let terminalStatus = '0'
    let statusChanged = '0'

    if(this.enableSwitch != this.previousLockStatus) {
      statusChanged = '1'
      if(this.enableSwitch == true) {
        reasonForStatus = 'Disconnected, Admin Lock.'
        terminalStatus = '2'
      } else {
        reasonForStatus = 'Connected, Admin Unlock'
        terminalStatus = '1'
      }
    } 
    this.terminalService.updateTerminal(terminalId, terminalName, ipAddress, macAddress, keepAliveTimer, branch, licenseId, status, statusChanged, reasonForStatus, terminalStatus)
      .subscribe(data => {
        this.notify.show(data.response, { position:'top', duration:'2000', type: 'success' });
        this.timeoutId =  setTimeout((router: Router) => {
          this.router.navigate(['/terminals']);
        }, 2000);
      }, error => {
        this.notify.show(error, { position:'top', duration:'5000', type: 'error' });
      })
  }

  serialNumberUpdated(data) {
    this.terminalService.updateDeviceSerialNumber(data.terminalDeviceId, data.serialNumber)
      .first()
      .subscribe(data => {
       this.notify.show(data.response, { position:'top', duration:'2000', type: 'success' });
          this.updateDevices();
      }, error => {
        this.notify.show(error, { position:'top', duration:'5000', type: 'error' });
      })
  }

}
