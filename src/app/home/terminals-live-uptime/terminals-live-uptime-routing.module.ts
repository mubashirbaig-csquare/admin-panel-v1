import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { TerminalsLiveUptimeComponent } from './terminals-live-uptime.component';



const routes: Routes = [
    { path: '', component: TerminalsLiveUptimeComponent }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TerminalsLiveUptimeRoutingModule {}