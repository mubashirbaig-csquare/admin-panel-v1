import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { routerTransition } from '../../router.animations';
import { ReportsService } from '../../shared/services/reports.service';

@Component({
  selector: 'app-terminals-live-uptime',
  templateUrl: './terminals-live-uptime.component.html',
  styleUrls: ['./terminals-live-uptime.component.scss'],
  animations: [routerTransition()]
})
export class TerminalsLiveUptimeComponent implements OnInit, OnDestroy {

  alive: boolean; // used to unsubscribe from the TimerObservable when OnDestroy is called.
  interval: number;

  responseData = []
  loading = false
  error = ''

  constructor(private reportsService: ReportsService) {
    this.alive = true;
    this.interval = 10000;
  }

  ngOnInit() {
    this.loading = true
    this.reportsService.getTerminalsLiveUptimeData()
      .first()
      .subscribe(data => {
        this.responseData = data.response
        this.loading = false;
      }, error => {
        this.responseData = []
        this.error = error
        this.loading = false;
      })

    TimerObservable.create(10000, this.interval)
      .takeWhile(() => this.alive)
      .subscribe(() => {
        this.loading = true;
        this.reportsService.getTerminalsLiveUptimeData()
          .subscribe(data => {
            this.loading = false;
            this.error = ''
            this.responseData = data.response
          }, error => {
            this.responseData = []
            this.error = error
            this.loading = false;
          }
          )
      })
  }

  getTerminalDisconnectColorIfDisconnected(value) {
    if (value == 'Disconnected') {
      return "Tbl_2 disconnected-status"
    } else if (value == 1) {
      return "Tbl_2"
    }
  }

  ngOnDestroy() {
    this.alive = false; // switches your TimerObservable off
  }

}
