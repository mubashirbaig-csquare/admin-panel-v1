import { LoadingModule } from 'ngx-loading';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TerminalsLiveUptimeComponent } from './terminals-live-uptime.component';
import { TerminalsLiveUptimeRoutingModule } from './terminals-live-uptime-routing.module';
import { PageHeaderModule } from '../../shared/modules/index';


@NgModule({
    declarations: [
        TerminalsLiveUptimeComponent
    ],
    imports: [
        CommonModule,
        TerminalsLiveUptimeRoutingModule,
        PageHeaderModule,
        LoadingModule
    ]
})
export class TerminalsLiveUptimeModule {}