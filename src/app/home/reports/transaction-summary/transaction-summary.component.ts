import { Component } from '@angular/core';

import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-transaction-summary',
  templateUrl: './transaction-summary.component.html',
  styleUrls: ['./transaction-summary.component.scss'],
  animations: [routerTransition()]
})
export class TransactionSummaryComponent { }
