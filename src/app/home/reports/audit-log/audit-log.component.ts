import { Component } from '@angular/core';
import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-audit-log',
  templateUrl: './audit-log.component.html',
  styleUrls: ['./audit-log.component.scss'],
  animations: [routerTransition()]
})
export class AuditLogComponent{
}
