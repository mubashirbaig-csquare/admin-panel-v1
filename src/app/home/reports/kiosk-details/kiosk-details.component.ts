import { Component } from '@angular/core';

import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-kiosk-details',
  templateUrl: './kiosk-details.component.html',
  styleUrls: ['./kiosk-details.component.scss'],
  animations: [routerTransition()]
})
export class KioskDetailsComponent {
}
