import { Component } from '@angular/core';

import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-transaction-details',
  templateUrl: './transaction-details.component.html',
  styleUrls: ['./transaction-details.component.scss'],
  animations: [routerTransition()]
})
export class TransactionDetailsComponent {}
