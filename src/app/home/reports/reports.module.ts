import { NgxPaginationModule } from 'ngx-pagination';
import { LoadingModule } from 'ngx-loading';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AngularDateTimePickerModule } from 'angular2-datetimepicker';

import { ReportsRoutingModule } from './reports-routing.module';
import { PageHeaderModule } from './../../shared/modules';
import { AuditLogComponent } from './audit-log/audit-log.component';
import { ReportsComponent } from './reports.component';
import { DateComponent } from './shared-reports-components/date/date.component';
import { AlertLogComponent } from './alert-log/alert-log.component';
import { KioskDetailsComponent } from './kiosk-details/kiosk-details.component';
import { KioskSummaryComponent } from './kiosk-summary/kiosk-summary.component';
import { TransactionSummaryComponent } from './transaction-summary/transaction-summary.component';
import { TransactionDetailsComponent } from './transaction-details/transaction-details.component';
import { TableComponent } from './shared-reports-components/table/table.component';
import { SearchComponent } from './shared-reports-components/search/search.component';
import { ExportExcelComponent } from './shared-reports-components/export-excel/export-excel.component';
import { ReportsTemplateComponent } from './shared-reports-components/reports-template/reports-template.component';
import { TerminalsUptimeComponent } from './terminals-uptime/terminals-uptime.component';
import { TerminalsCriticalLogsComponent } from './terminals-critical-logs/terminals-critical-logs.component';
import { SearchAndExportExcelComponent } from './shared-reports-components/search-and-export-excel/search-and-export-excel.component';

@NgModule({
    declarations: [
        AuditLogComponent,
        ReportsComponent,
        DateComponent,
        AlertLogComponent,
        KioskDetailsComponent,
        KioskSummaryComponent,
        TransactionSummaryComponent,
        TransactionDetailsComponent,
        TableComponent,
        SearchComponent,
        ExportExcelComponent,
        ReportsTemplateComponent,
        TerminalsUptimeComponent,
        TerminalsCriticalLogsComponent,
        SearchAndExportExcelComponent,
    ],
    imports: [
        CommonModule,
        ReportsRoutingModule,
        FormsModule,
        LoadingModule,
        NgxPaginationModule,
        NgbModule.forRoot(),
        PageHeaderModule,
        Ng2SearchPipeModule,
        AngularDateTimePickerModule
    ]
})
export class ReportsModule { }