import { Component, OnInit } from '@angular/core';

import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-terminals-uptime',
  templateUrl: './terminals-uptime.component.html',
  styleUrls: ['./terminals-uptime.component.scss'],
  animations: [routerTransition()]
})
export class TerminalsUptimeComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
