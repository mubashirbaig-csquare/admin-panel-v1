import { Component, OnInit } from '@angular/core';

import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-terminals-critical-logs',
  templateUrl: './terminals-critical-logs.component.html',
  styleUrls: ['./terminals-critical-logs.component.scss'],
  animations: [routerTransition()]
})
export class TerminalsCriticalLogsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
