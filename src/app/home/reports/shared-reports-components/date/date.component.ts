import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { ReportsService } from '../../../../shared/services/reports.service';
import { DatePicker } from 'angular2-datetimepicker';

@Component({
  selector: 'app-date',
  templateUrl: './date.component.html',
  styleUrls: ['./date.component.scss']
})
export class DateComponent implements OnInit {

  date: Date = new Date();
  settings = {
    bigBanner: true,
    timePicker: true,
    format: 'dd-MMM-yyyy hh:mm a',
    defaultOpen: false
  }
  toDate: Date = new Date();
  fromDate: Date = new Date();
  display
  loading: boolean
  @Output() reportsFetched = new EventEmitter()
  @Input() serviceToBeCalled

  constructor(private reportsService: ReportsService) {
    this.display = false
    this.loading = false
    DatePicker.prototype.ngOnInit = function () {
      this.settings = Object.assign(this.defaultSettings, this.settings);
      if (this.settings.defaultOpen) {
        this.popover = true;
      }
      this.date = new Date();
      this.toDate = new Date();
      this.fromDate = new Date();
    };
  }

  ngOnInit() { }

  onFormSubmit() {
    this.loading = true
    const splittedFromDate = this.fromDate.toString().split(' ')
    const splittedToDate = this.toDate.toString().split(' ')
    const formattedFromDate = `${splittedFromDate[3]}-${this.getMonthNumber(splittedFromDate[1])}-${splittedFromDate[2]} ${splittedFromDate[4]}`
    const formattedToDate = `${splittedToDate[3]}-${this.getMonthNumber(splittedToDate[1])}-${splittedToDate[2]} ${splittedToDate[4]}`

    switch (this.serviceToBeCalled) {
      case 'audit-log': {
        this.auditLogService(formattedFromDate, formattedToDate)
        break
      }
      case 'alert-log': {
        this.alertLogService(formattedFromDate, formattedToDate)
        break
      }
      case 'kiosk-details': {
        this.kioskDetailsService(formattedFromDate, formattedToDate)
        break
      }
      case 'kiosk-summary': {
        this.kioskSummaryService(formattedFromDate, formattedToDate)
        break
      }
      case 'transaction-details': {
        this.transactionDetailsService(formattedFromDate, formattedToDate)
        break
      }
      case 'transaction-summary': {
        this.transactionSummaryService(formattedFromDate, formattedToDate)
        break
      }
      case 'terminals-uptime': {
        this.terminalUptimeService(formattedFromDate, formattedToDate)
        break
      }
      case 'terminals-critical-logs': {
        this.terminalCriticalLogsService(formattedFromDate, formattedToDate)
        break
      }
    }
    //  setTimeout(() => {    //<<<---    using ()=> syntax
    // }, 10000);
  }

  auditLogService(formattedFromDate, formattedToDate) {
    this.reportsService.getAuditLogData(formattedFromDate, formattedToDate)
      .subscribe(data => {
        this.loading = false
        this.reportsFetched.emit({ data: data.response, error: null })
      }, error => {
        this.loading = false
        this.reportsFetched.emit({ data: null, error })
      })
  }

  alertLogService(formattedFromDate, formattedToDate) {
    this.reportsService.getAlertsLogData(formattedFromDate, formattedToDate)
      .subscribe(data => {
        this.loading = false
        this.reportsFetched.emit({ data: data.response, error: null })
      }, error => {
        this.loading = false
        this.reportsFetched.emit({ data: null, error })
      })
  }

  kioskDetailsService(formattedFromDate, formattedToDate) {
    this.reportsService.getKioskDetailsData(formattedFromDate, formattedToDate)
      .subscribe(data => {
        this.loading = false
        this.reportsFetched.emit({ data: data.response, error: null })
      }, error => {
        this.loading = false
        this.reportsFetched.emit({ data: null, error })
      })
  }

  kioskSummaryService(formattedFromDate, formattedToDate) {
    this.reportsService.getKioskSummaryData(formattedFromDate, formattedToDate)
      .subscribe(data => {
        this.loading = false
        this.reportsFetched.emit({ data: data.response, error: null })
      }, error => {
        this.loading = false
        this.reportsFetched.emit({ data: null, error })
      })
  }

  transactionSummaryService(formattedFromDate, formattedToDate) {
    this.reportsService.getTransactionSummaryData(formattedFromDate, formattedToDate)
      .subscribe(data => {
        this.loading = false
        this.reportsFetched.emit({ data: data.response, error: null })
      }, error => {
        this.loading = false
        this.reportsFetched.emit({ data: null, error })
      })
  }

  transactionDetailsService(formattedFromDate, formattedToDate) {
    this.reportsService.getTransactionDetailsData(formattedFromDate, formattedToDate)
      .subscribe(data => {
        this.loading = false
        this.reportsFetched.emit({ data: data.response, error: null })
      }, error => {
        this.loading = false
        this.reportsFetched.emit({ data: null, error })
      })
  }

  terminalUptimeService(formattedFromDate, formattedToDate) {
    this.reportsService.getTerminalUptimeData(formattedFromDate, formattedToDate)
      .subscribe(data => {
        this.loading = false
        this.reportsFetched.emit({ data: data.response, error: null })
      }, error => {
        this.loading = false
        this.reportsFetched.emit({ data: null, error })
      })
  }

  terminalCriticalLogsService(formattedFromDate, formattedToDate) {
    this.reportsService.getTerminalCriticalLogsData(formattedFromDate, formattedToDate)
      .subscribe(data => {
        this.loading = false
        this.reportsFetched.emit({ data: data.response, error: null })
      }, error => {
        this.loading = false
        this.reportsFetched.emit({ data: null, error })
      })
  }

  getMonthNumber(monthName) {
    switch (monthName) {
      case 'Jan': {
        return "1"
      }
      case 'Feb': {
        return "2"
      }
      case 'Mar': {
        return "3"
      }
      case 'Apr': {
        return "4"
      }
      case 'May': {
        return "5"
      }
      case 'Jun': {
        return "6"
      }
      case 'Jul': {
        return "7"
      }
      case 'Aug': {
        return "8"
      }
      case 'Sep': {
        return "9"
      }
      case 'Oct': {
        return "10"
      }
      case 'Nov': {
        return "11"
      }
      case 'Dec': {
        return "12"
      }
    }
  }

}
