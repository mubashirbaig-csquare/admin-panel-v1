import { Ng2SearchPipe } from 'ng2-search-filter';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import {CsvService} from "angular2-json2csv";

import { ExcelService } from '../../../../shared/services/excel.service';

@Component({
  selector: 'app-search-and-export-excel',
  templateUrl: './search-and-export-excel.component.html',
  styleUrls: ['./search-and-export-excel.component.scss']
})
export class SearchAndExportExcelComponent {

  // @Input() searchKeyWords = ''
  @Input() dataToBeExported = []
  @Input() fileName = ''
  @Output() filteredTerm = new EventEmitter<string>()
  searchTerm = ''
  
  constructor(private excelService: ExcelService, private csvService: CsvService) { }

  exportToExcel() {
    const filteredData = new Ng2SearchPipe().transform(this.dataToBeExported, this.searchTerm)
    // this.excelService.exportAsExcelFile(filteredData, this.fileName);
    this.csvService.download(filteredData, this.fileName + '-' + new Date());
  }

  onKeyPress(data) {
    this.filteredTerm.emit(data)
  }

  clearSearch() {
    this.searchTerm = ''
    this.filteredTerm.emit(this.searchTerm)
  }

}
