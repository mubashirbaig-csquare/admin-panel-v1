import { Component, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent {

  @Output() filteredTerm = new EventEmitter<string>()
  searchTerm = ''

  constructor() { }

  onKeyPress(data) {
    this.filteredTerm.emit(data)
  }

  clearSearch() {
    this.searchTerm = ''
    this.filteredTerm.emit(this.searchTerm)
  }

}
