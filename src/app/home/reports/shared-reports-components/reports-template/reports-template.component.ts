import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-reports-template',
  templateUrl: './reports-template.component.html',
  styleUrls: ['./reports-template.component.scss']
})
export class ReportsTemplateComponent {

  auditLogData = []
  error = ''
  searchTerm = ''
  @Input() tableHeadersArray = []
  @Input() objectProps = []
  @Input() fileName = ''
  @Input() service = ''
  
  onGetReportsData(serverData) {
    this.error = ''
    if(serverData.error == null) {
      this.auditLogData = serverData.data
    } else {
      this.auditLogData = []
      this.error = serverData.error
    }
  }

  onFilterData(data) {
    this.searchTerm = data
  }

}
