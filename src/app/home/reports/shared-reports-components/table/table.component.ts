import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent {
  
  @Input() tableHeaders
  @Input() inputData
  @Input() dataKeys
  @Input() searchKeyWords
  @Input() errorMsg

}
