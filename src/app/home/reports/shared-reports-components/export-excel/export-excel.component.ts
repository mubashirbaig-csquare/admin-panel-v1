import { Ng2SearchPipe } from 'ng2-search-filter';
import { Component, Input } from '@angular/core';
import { ExcelService } from '../../../../shared/services/excel.service';
import {CsvService} from "angular2-json2csv";

@Component({
  selector: 'app-export-excel',
  templateUrl: './export-excel.component.html',
  styleUrls: ['./export-excel.component.scss']
})
export class ExportExcelComponent {
  @Input() searchKeyWords = ''
  @Input() dataToBeExported = []
  @Input() fileName = ''

  constructor(private excelService: ExcelService, private csvService: CsvService) { }

  exportToExcel() {
    const filteredData = new Ng2SearchPipe().transform(this.dataToBeExported, this.searchKeyWords)
    // this.excelService.exportAsExcelFile(filteredData, this.fileName);
    this.csvService.download(filteredData, 'Filename');
  }

}
