import { Component } from '@angular/core';

import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-kiosk-summary',
  templateUrl: './kiosk-summary.component.html',
  styleUrls: ['./kiosk-summary.component.scss'],
  animations: [routerTransition()]
})
export class KioskSummaryComponent {}
