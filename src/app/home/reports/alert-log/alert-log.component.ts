import { Component } from '@angular/core';

import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-alert-log',
  templateUrl: './alert-log.component.html',
  styleUrls: ['./alert-log.component.scss'],
  animations: [routerTransition()]
})
export class AlertLogComponent {

}
