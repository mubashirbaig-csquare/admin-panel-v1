import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { AuditLogComponent } from './audit-log/audit-log.component';
import { ReportsComponent } from './reports.component';
import { AlertLogComponent } from './alert-log/alert-log.component';
import { KioskDetailsComponent } from './kiosk-details/kiosk-details.component';
import { KioskSummaryComponent } from './kiosk-summary/kiosk-summary.component';
import { TransactionSummaryComponent } from './transaction-summary/transaction-summary.component';
import { TransactionDetailsComponent } from './transaction-details/transaction-details.component';
import { TerminalsUptimeComponent } from './terminals-uptime/terminals-uptime.component';
import { TerminalsCriticalLogsComponent } from './terminals-critical-logs/terminals-critical-logs.component';

const routes: Routes = [
    { path: '', component: ReportsComponent, children: [
        { path: 'audit-log', component: AuditLogComponent },
        { path: 'alert-logs', component: AlertLogComponent },
        { path: 'kiosk-details', component: KioskDetailsComponent },
        { path: 'kiosk-summary', component: KioskSummaryComponent },
        { path: 'transaction-details', component: TransactionDetailsComponent },
        { path: 'transaction-summary', component: TransactionSummaryComponent },
        { path: 'terminal-uptime', component: TerminalsUptimeComponent },
        { path: 'terminal-critical-logs', component: TerminalsCriticalLogsComponent },
        // { path: 'permissions', component: UserPermissionsComponent }
    ] }
]

@NgModule({
    exports: [RouterModule],
    imports: [RouterModule.forChild(routes)]
})
export class ReportsRoutingModule {}