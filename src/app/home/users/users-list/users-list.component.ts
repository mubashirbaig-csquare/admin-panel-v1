import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { UserService } from '../../../shared/services/user.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss'],
  animations: [routerTransition()]
})
export class UsersListComponent implements OnInit {

  searchTerm = ''
  display: boolean
  userErrorMessage = ''
  users = []
  loading = false


  constructor(private userService: UserService) { 
    this.display = true;
  }

  ngOnInit() {
    this.loading = true
    this.userService.getUsers()
    .first() // only gets fired once
    .subscribe((data) => {
      this.users = data.response
      this.loading = false
      this.display = true;
    }, error => {
      this.userErrorMessage = error
      this.loading = false
      this.display = false;
    });
  }

  clearSearch() {
    this.searchTerm = ''
  }

}
