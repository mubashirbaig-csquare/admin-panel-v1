import { NotificationService } from 'ng2-notify-popup';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { NgForm } from '@angular/forms';

import { UserService } from '../../../shared/services/user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
  animations: [routerTransition()]
})


export class UserComponent implements OnInit, OnDestroy {
  @ViewChild('f') userForm: NgForm

  roles = []
  editMode = false
  userId = ''
  rolesArray = []
  editRolesValues = []
  error = ""
  success = ""
  enableSwitch = false
  /**
   *  Variables for populating user data when form loaded in edit mode
  */
  username = ''
  fullname = ''
  email = ''
  mobile = ''
  timeoutId: any
  loading = false

  constructor(private route: ActivatedRoute, private router: Router, private userService: UserService, private notify: NotificationService) { }

  ngOnInit() {
    this.setRolesFromLocalStorage()
    this.route.params
      .subscribe(
      (params: any) => {
        this.userId = params['id'];
        if (this.userId !== undefined) {
          this.editMode = true
        }
      })

    if (this.editMode) {
      this.populatingFormInEditMode();
    }

  }

  private populatingFormInEditMode() {
    this.loading = true
    this.userService.getUserById(this.userId)
      .subscribe(data => {      
        this.userForm.form.patchValue({
          username: data.response.username,
          fullname: data.response.fullname,
          email: data.response.email,
          mobile: data.response.mobile
        });
        this.enableSwitch = data.response.userStatus == 1 ? true : false
        this.rolesArray = [];
        this.editRolesValues = [];
        for (let role of this.roles) {
          this.editRolesValues.push({ roleId: role.roleId, roleName: role.roleName, checked: false });
        }
        for (let role of this.editRolesValues) {
          for (let responseRole of data.response.roles) {
            if (responseRole.roleId == role.roleId) {
              this.rolesArray.push(role.roleId.toString());
              role.checked = true;
            }
          }
        }
        this.loading = false  
      }, err => {
        this.loading = false
        this.notify.show(err, { position:'top', duration:'5000', type: 'error' });
      });
  }

  private setRolesFromLocalStorage() {
    if (JSON.parse(localStorage.getItem('roles'))) {
      this.roles = JSON.parse(localStorage.getItem('roles'));
    }
    else {
      this.userService.getRoles()
        .subscribe(data => {
          localStorage.setItem('roles', JSON.stringify(data.response));
          this.roles = JSON.parse(localStorage.getItem('roles'));
        }, err => { this.router.navigate(['/']); });
    }
  }

  onUserFormSubmit() {
    this.error = ''
    this.success = ''
    if (this.userForm.valid && this.rolesArray.length > 0) {
      const username = this.userForm.controls['username'].value
      const fullname = this.userForm.controls['fullname'].value
      const email = this.userForm.controls['email'].value
      const mobile = this.userForm.controls['mobile'].value
      const roles = this.rolesArray
      if (!this.editMode) {
        this.addNewUser(username, fullname, email, mobile, roles)
      } else {
        const userStatus = this.enableSwitch == true ? "1" : "0"
        this.updateNewUser(username, fullname, email, mobile, roles, userStatus)
      }

    } else {
      this.notify.show('Form is incomplete.', { position:'top', duration:'5000', type: 'error' });
    }
  }

  onUserFormReset() {
    this.rolesArray = []
  }

  onUserRoleCheckBoxChange(data) {
    if (data.target.checked) {
      this.rolesArray.push(data.target.value)
    } else {
      const index = this.rolesArray.indexOf(data.target.value, 0);
      if (index > -1) {
        this.rolesArray.splice(index, 1)
      }
    }
  }

  addNewUser(username, fullname, email, mobile, roles) {
    this.loading = true
    this.userService.insertUser(username, fullname, email, mobile, roles)
      .subscribe(data => {
        this.loading = false
        this.notify.show(data.response, { position:'top', duration:'2000', type: 'success' });        
        this.userForm.reset()
        this.rolesArray = [] 
        this.timeoutId =  setTimeout((router: Router) => {
          this.router.navigate(['/users']);
        }, 2000);
      }, error => {
        this.loading = false  
        this.notify.show(error, { position:'top', duration:'5000', type: 'error' });
      }
      )
  }

  updateNewUser(username, fullname, email, mobile, roles, isEnabled) {
    this.loading = true
    this.userService.updateUser(username, fullname, email, mobile, roles, this.userId, isEnabled)
      .subscribe(data => {
        this.loading = false  
        this.notify.show(data.response, { position:'top', duration:'2000', type: 'success' });
        this.timeoutId =  setTimeout((router: Router) => {
          this.router.navigate(['/users']);
        }, 2000);
      }, error => {
        this.loading = false  
        this.notify.show(error, { position:'top', duration:'5000', type: 'error' });
      }
      )
  }

  onResetPassword() {
    this.loading = true
    const email = this.userForm.controls['email'].value
    if(email != '') {
      this.userService.resetPassword(this.userId, email)
      .subscribe(data => {
        this.loading = false  
        this.notify.show(data.response, { position:'top', duration:'2000', type: 'success' });
        this.timeoutId =  setTimeout((router: Router) => {
          this.router.navigate(['/users']);
        }, 2000);
      }, error => {
        this.loading = false  
        this.notify.show(error, { position:'top', duration:'5000', type: 'error' });
      })
    } else {
      this.notify.show('Email not valid !', { position:'top', duration:'5000', type: 'error' });
    }
    
  }

  onChange(data) {
    this.enableSwitch = data
  }

  ngOnDestroy() {
    clearTimeout(this.timeoutId)
  }

}
