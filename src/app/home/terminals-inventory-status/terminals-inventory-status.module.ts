import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LoadingModule } from 'ngx-loading';


import { PageHeaderModule } from './../../shared/modules/page-header/page-header.module';
import { PipeModule } from '../../shared/pipes/pipes.module';
import { TerminalsInventoryStatusRoutingModule } from './terminals-inventory-status.routing-module';
import { TerminalsInventoryStatusComponent } from './terminals-inventory-status.component';

@NgModule({
    declarations: [TerminalsInventoryStatusComponent],
    imports: [
        CommonModule,
        TerminalsInventoryStatusRoutingModule,
        PageHeaderModule,
        LoadingModule
    ]
})
export class TerminalsInventoryStatusModule {}