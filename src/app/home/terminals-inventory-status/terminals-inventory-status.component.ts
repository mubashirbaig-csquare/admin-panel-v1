import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { ReportsService } from '../../shared/services/reports.service';

@Component({
  selector: 'app-terminals-inventory-status',
  templateUrl: './terminals-inventory-status.component.html',
  styleUrls: ['./terminals-inventory-status.component.scss'],
  animations: [routerTransition()]
})
export class TerminalsInventoryStatusComponent implements OnInit {

  dataToDisplay = []
  loading
  error

  constructor(private reportsService: ReportsService) { }

  ngOnInit() {
    this.loading = true;
    this.reportsService.getTerminalsInventoryStatusData()
      .first() // only gets fired once
      .subscribe(data => {
        this.dataToDisplay = data.response;
        this.loading = false;
      }, error => {
        this.error = error
        this.loading = false;
      });
  }

}
