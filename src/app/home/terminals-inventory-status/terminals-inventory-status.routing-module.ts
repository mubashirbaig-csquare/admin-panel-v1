import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { TerminalsInventoryStatusComponent } from './terminals-inventory-status.component';

const routes: Routes = [
    { path: '', component: TerminalsInventoryStatusComponent }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TerminalsInventoryStatusRoutingModule { }