import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { EmailNotificationsComponent } from './email-notifications.component';
import { EmailNotificationsListComponent } from './email-notifications-list/email-notifications-list.component';
import { EditEmailNotificationsComponent } from './edit-email-notifications/edit-email-notifications.component';

const routes: Routes = [
    { path: '', component: EmailNotificationsListComponent },
    { path: ':id/edit', component: EditEmailNotificationsComponent }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class EmailNotificationsRoutingModule {}