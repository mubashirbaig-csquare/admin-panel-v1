import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { EmailNotificationsService } from '../../../shared/services/email-notifications.service';

@Component({
  selector: 'app-email-notifications-list',
  templateUrl: './email-notifications-list.component.html',
  styleUrls: ['./email-notifications-list.component.scss'],
  animations: [routerTransition()]
})
export class EmailNotificationsListComponent implements OnInit {

  searchTerm = ''
  display: boolean
  userErrorMessage = ''
  emailNotificationsList = []
  loading = false


  constructor(private emailNotificationsService: EmailNotificationsService) { 
    this.display = true;
  }

  ngOnInit() {
    this.loading = true
    this.emailNotificationsService.getNotificationsList()
    .first() // only gets fired once
    .subscribe((data) => {
      this.emailNotificationsList = data.response
      this.loading = false
      this.display = true;
    }, error => {
      this.userErrorMessage = error
      this.loading = false
      this.display = false;
    });
  }

  clearSearch() {
    this.searchTerm = ''
  }

}
