import { NgForm } from '@angular/forms';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { routerTransition } from '../../../router.animations';
import { EmailNotificationsService } from '../../../shared/services/email-notifications.service';
import { NotificationService } from 'ng2-notify-popup';

@Component({
  selector: 'app-edit-email-notifications',
  templateUrl: './edit-email-notifications.component.html',
  styleUrls: ['./edit-email-notifications.component.scss'],
  animations: [routerTransition()]
})
export class EditEmailNotificationsComponent implements OnInit,OnDestroy {

  
  notificationAlert
  emailTo
  emailCC
  error = ""
  timeoutId: any
  success = ""


  @ViewChild('f') emailNotificationsForm: NgForm

  constructor(private route: ActivatedRoute,
    private router: Router,
    private emailNotificationsService: EmailNotificationsService,
    private notify: NotificationService) { }

  ngOnInit() {
    this.route.params
    .subscribe(
    (params: any) => {
      this.notificationAlert = params['id'];
      this.populatingFormInEditMode();
    })
  }

  private populatingFormInEditMode() {
    this.emailNotificationsService.getNotificationDetailByName(this.notificationAlert)
    .subscribe(data => {      
      console.log(data);
      this.emailNotificationsForm.form.patchValue({
        alertName: data.response[0].NotificationAlert,
        emailTo: data.response[0].EmailTo,
        emailCC: data.response[0].EmailCC,
      });
    }, err => {
      this.notify.show(err, { position:'top', duration:'5000', type: 'error' });
    });
  }

  ngOnDestroy() {
     clearTimeout(this.timeoutId)
  }

  

  updateNotificationEmails(notificationAlert, emailTo, emailCC) {
    this.emailNotificationsService.updateNotificationEmails(notificationAlert, emailTo, emailCC)
      .subscribe(data => { 
        this.notify.show(data.response, { position:'top', duration:'2000', type: 'success' });
        this.timeoutId =  setTimeout((router: Router) => {
          this.router.navigate(['/email-notifications']);
        }, 2000);
      }, error => {
        this.notify.show(error, { position:'top', duration:'5000', type: 'error' });
      }
      )
  }

  onNotificationEmailFormSubmit() {    
    this.error = ''
    this.success = ''
    if (this.emailNotificationsForm.valid) {
      const alertType = this.emailNotificationsForm.controls['alertName'].value
      const emailTo = this.emailNotificationsForm.controls['emailTo'].value
      const emailCC = this.emailNotificationsForm.controls['emailCC'].value == null ? "" : this.emailNotificationsForm.controls['emailCC'].value
  
      this.updateNotificationEmails(alertType, emailTo, emailCC)
    }
    else {
      this.notify.show('Form is incomplete.', { position:'top', duration:'5000', type: 'error' });
    }
  }

 
 

}
