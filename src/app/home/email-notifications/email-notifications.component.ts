import { Component, OnInit, OnDestroy } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { MonitorService } from '../../shared/services/monitor.service';
import { Response } from '@angular/http'
import { TimerObservable } from 'rxjs/observable/TimerObservable';

@Component({
  selector: 'app-monitoring',
  templateUrl: './email-notifications.component.html',
  styleUrls: ['./email-notifications.component.scss'],
  animations: [routerTransition()]
})
export class EmailNotificationsComponent implements OnInit, OnDestroy {


  constructor(private monitorService: MonitorService) {
    
  }

  ngOnInit() {
    
    
  }


  ngOnDestroy() {
  }
}
