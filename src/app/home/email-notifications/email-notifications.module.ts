import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { LoadingModule } from 'ngx-loading';
import { FormsModule } from '@angular/forms';

import { EmailNotificationsRoutingModule } from './email-notifications-routing.module';
import { PageHeaderModule } from './../../shared/modules/page-header/page-header.module';
import { EmailNotificationsComponent } from './email-notifications.component';
import { PipeModule } from '../../shared/pipes/pipes.module';
import { EmailNotificationsListComponent } from './email-notifications-list/email-notifications-list.component';
import { EditEmailNotificationsComponent } from './edit-email-notifications/edit-email-notifications.component';

@NgModule({
    declarations: [
        EmailNotificationsComponent, 
        EmailNotificationsListComponent,
        EditEmailNotificationsComponent
        ],
    imports: [
        CommonModule,
        EmailNotificationsRoutingModule,
        PageHeaderModule,
        LoadingModule,
        FormsModule,
        PipeModule,
        NgbModule.forRoot()
    ]
})
export class EmailNotificationsModule {}