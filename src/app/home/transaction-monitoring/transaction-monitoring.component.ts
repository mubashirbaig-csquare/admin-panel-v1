import { TimerObservable } from 'rxjs/observable/TimerObservable';
import { Component, OnInit, OnDestroy } from '@angular/core';

import { TransactionService } from '../../shared/services/transaction.service';
import { routerTransition } from '../../router.animations';
import { MonitorService } from '../../shared/services/monitor.service';

@Component({
  selector: 'app-transaction-monitoring',
  templateUrl: './transaction-monitoring.component.html',
  styleUrls: ['./transaction-monitoring.component.scss'],
  animations: [routerTransition()]
})
export class TransactionMonitoringComponent implements OnInit, OnDestroy {
  accountOpeningCount = ''
  cardDesposedCount = ''
  totalCashDepositCount = ''
  monitoringData = []

  display: boolean; // whether to display info in the component
  // use *ngIf="display" in your html to take
  // advantage of this
  loading = false;
  alive: boolean; // used to unsubscribe from the TimerObservable
  // when OnDestroy is called.
  interval: number;
  userErrorMessage: string = ''

  constructor(private transactionService: TransactionService, private monitorService: MonitorService) {
    this.display = true;
    this.alive = true;
    this.interval = 10000;
  }

  ngOnInit() {

    this.transactionService.getTransactionsMonitoringData()
      .first() // only gets fired once
      .subscribe(data => {
        this.accountOpeningCount = data.response.AccOpenCount;
        this.cardDesposedCount = data.response.CardDespCount;
        this.totalCashDepositCount = data.response.TotCashDeposit;
        this.loading = false;
        this.display = true;
      }, error => {
        this.userErrorMessage = error
        this.display = false;
        this.loading = false;
      });

    TimerObservable.create(10000, this.interval)
      .takeWhile(() => this.alive)
      .subscribe(() => {
        this.loading = true;
        this.transactionService.getTransactionsMonitoringData()
          .subscribe(data => {
            this.accountOpeningCount = data.response.AccOpenCount;
            this.cardDesposedCount = data.response.CardDespCount;
            this.totalCashDepositCount = data.response.TotCashDeposit;
            this.loading = false;
            this.display = true;
          }, error => {
            this.userErrorMessage = error
            this.display = false;
            this.loading = false;
          }
          )
      })

    this.monitorService.getTerminalsMonitoringData()
      .first() // only gets fired once
      .subscribe(data => {
        this.monitoringData = data.response;
        this.loading = false;
        this.display = true;
      }, error => {
        this.userErrorMessage = error
        this.display = false;
        this.loading = false;
      });

    TimerObservable.create(10000, this.interval)
      .takeWhile(() => this.alive)
      .subscribe(() => {
        this.loading = true;
        this.monitorService.getTerminalsMonitoringData()
          .subscribe(data => {
            this.loading = false;
            this.monitoringData = data.response
          }, error => {
            this.userErrorMessage = error
            this.display = false;
            this.loading = false;
          }
          )
      })
  }

  getStyle(temp: any) {
    if (temp == '0') {
      return "btn btn-sm btn-success"
    } else if (temp == '1') {
      return "btn btn-sm btn-warning"
    } else if (temp == '2') {
      return "btn btn-sm btn-danger"
    }
  }

  getOverallTerminalStatusColor(value) {
    if (value == 0) {
      return "rgb(238, 58, 58)"
    } else if (value == 1) {
      return "#88cb88"
    }
  }

  getTerminalConnectionImageBasedOnDatabaseValue(value) {
    if (value == 0) {
      return "fa fa-chain-broken DMIcon-red"
    } else if (value == 1) {
      return "fa fa-chain DMIcon-green"
    }
  }

  ngOnDestroy() {
    this.alive = false; // switches your TimerObservable off
  }
}