import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { TransactionMonitoringComponent } from './transaction-monitoring.component';

const routes: Routes = [
    { path: '', component: TransactionMonitoringComponent }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class TransactionMonitoringRoutingModule {}