import { LoadingModule } from 'ngx-loading';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { TransactionMonitoringComponent } from './transaction-monitoring.component';
import { PageHeaderModule } from '../../shared/modules/index';
import { TransactionMonitoringRoutingModule } from './transaction-monitoring-routing.module';
import { PipeModule } from '../../shared/pipes/pipes.module';


@NgModule({
    imports: [
        CommonModule,
        PageHeaderModule,
        TransactionMonitoringRoutingModule,
        PipeModule,
        LoadingModule
    ],
    declarations: [TransactionMonitoringComponent]
})
export class TransactionMonitoringModule {}