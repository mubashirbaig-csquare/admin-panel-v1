import { NotificationService } from 'ng2-notify-popup';
import { Router } from '@angular/router';
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { NgForm } from '@angular/forms';
import { PasswordService } from '../../shared/services/password.service';
import { AuthService } from '../../shared/services/auth.service';

@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: ['./password.component.scss'],
  animations: [routerTransition()]
})
export class PasswordComponent implements OnInit, OnDestroy {

  @ViewChild('f') changePasswordForm: NgForm
  timeoutId: any
  loading = false

  constructor(private router: Router, private passwordService: PasswordService, private notify: NotificationService) { }

  ngOnInit() { }

  onSubmit() {
    this.loading = true
    const userId = JSON.parse(localStorage.getItem('auth')).userId
    const password = this.changePasswordForm.form.value.password
    if (this.changePasswordForm.valid) {
      this.passwordService.updatePassword(password, userId)
        .subscribe(data => {
          this.loading = false
          this.notify.show(data.response, { position: 'top', duration: '2000', type: 'success' });
          this.timeoutId = setTimeout((router: Router) => {
            this.router.navigate(['/']);
          }, 2000);
        }, error => {
          this.loading = false
          this.notify.show(error, { position: 'top', duration: '5000', type: 'error' });
        })
    }
  }

  ngOnDestroy() {
    clearTimeout(this.timeoutId)
  }

}
