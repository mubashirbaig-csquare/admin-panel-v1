import { CsvService } from 'angular2-json2csv';
import { JwtInterceptor } from './shared/services/jwt.interceptor';
import { NgModule } from '@angular/core';
import { Http, HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NgNotifyPopup } from 'ng2-notify-popup';
import {ToastModule} from 'ng2-toastr/ng2-toastr';

import { AppComponent } from './app.component';
import { TerminalService } from './shared/services/terminal.service';
import { UserService } from './shared/services/user.service';
import { MonitorService } from './shared/services/monitor.service';
import { AppRoutingModule } from './app-routing.module';
import { PasswordService } from './shared/services/password.service';
import { AuthService } from './shared/services/auth.service';
import { AuthGuard } from './shared/guard/auth.guard';
import { ServiceUrl } from './shared/services/servicesUrl';
import { TokenInterceptor } from './shared/services/token.interceptor';
import { TransactionService } from './shared/services/transaction.service';
import { ReportsService } from './shared/services/reports.service';
import { ExcelService } from './shared/services/excel.service';
import { EmailNotificationsService } from './shared/services/email-notifications.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    // FormsModule,
    HttpClientModule,
    HttpModule,
    AppRoutingModule,
    NgNotifyPopup.forRoot(),
    ToastModule.forRoot()
  ],
  providers: [
    MonitorService, 
    TerminalService, 
    UserService, 
    PasswordService, 
    AuthService, 
    AuthGuard, 
    ServiceUrl,
    TransactionService,
    ReportsService,
    ExcelService,
    CsvService,
    EmailNotificationsService,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true }
  //  TokenInterceptor,
  //  JwtInterceptor
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
